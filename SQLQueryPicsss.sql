﻿use AutomationEngineeringDevicesStorage

--update Products

--set Picture=
--( select * from openrowset (BULK N'‪C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 1.PNG', SINGLE_BLOB) AS Image)
--WHERE Products.ProductName= 'Hotel Room Controller';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 2.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'ECLYPSE Connected VAV Controller – ECY-VAV Series';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 3.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'AmpLight - Centralized Streetlight Control';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 4.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Spyder Controller';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 5.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'NX-M Series Master Control Panel';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 6.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Battery Backup';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 7.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Water Leakage Detecting System';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 8.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Locator Tag';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 9.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Series 930 – Fixed Indoor Air Quality Monitor';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Home Automation\Home Automation 10.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Series 500 – Portable Ozone Monitor';

---------------

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 1.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Truevolt Series 6? Digit Digital Multimeter';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 2.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= '4200-SCS Semiconductor Parameter Analyzer';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 3.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= '2182A Nanovoltmeter';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 4.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Voltmeter/Ammeter Case';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 5.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Electrothermal Digital Heating Controller';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 6.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'DigiTrol II Temperature Controls';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 7.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Digital Timer';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 8.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'Guardian NG DC Gas Monitor';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 9.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'FASTCAM 1024 PCI High Speed Camera';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Laboratory Automation\Lab Automation 10.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'iKon-L 936 CCD camera';

--------------

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Industrial Automation\Industrial Automation 1.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'DirectLOGIC 305 Programmable Logic Controller';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Industrial Automation\Industrial Automation 2.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'X-Rail';

update Products
set Picture=
( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Industrial Automation\Industrial Automation 4.PNG', SINGLE_BLOB) AS Image)
WHERE Products.ProductName= 'SolaHD SDU AC - A Series DIN Rail UPS';

--update Products
--set Picture=
--( select * from openrowset (BULK N'C:\Users\User\Desktop\Pictures\Industrial Automation\Industrial Automation 5.PNG', SINGLE_BLOB) AS Image)
--WHERE Products.ProductName= '*7” Widescreen HMI | Slim Design';

