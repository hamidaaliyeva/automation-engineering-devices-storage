USE [master]
GO
/****** Object:  Database [AutomationEngineeringDevicesStorage]    Script Date: 25-May-18 7:59:14 PM ******/
CREATE DATABASE [AutomationEngineeringDevicesStorage]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AutomationEngineeringDevicesStorage', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\AutomationEngineeringDevicesStorage.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AutomationEngineeringDevicesStorage_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\AutomationEngineeringDevicesStorage_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AutomationEngineeringDevicesStorage].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET ARITHABORT OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET  MULTI_USER 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [AutomationEngineeringDevicesStorage]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 25-May-18 7:59:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryName] [nchar](50) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Companies]    Script Date: 25-May-18 7:59:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[Company] [nchar](50) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Deals]    Script Date: 25-May-18 7:59:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deals](
	[SellerID] [int] NULL,
	[Buyer] [int] NULL,
	[Price] [float] NULL,
	[ProductID] [int] NULL,
	[Date] [date] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Deals] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 25-May-18 7:59:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Products](
	[ProductName] [nchar](50) NULL,
	[Brand] [int] NULL,
	[Description] [text] NULL,
	[Country] [nchar](50) NULL,
	[Price] [float] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Picture] [varbinary](max) NULL,
	[Category] [int] NULL,
	[Amount] [int] NULL,
	[Seller] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 25-May-18 7:59:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Name] [nchar](50) NULL,
	[Surname] [nchar](50) NULL,
	[Email] [nchar](50) NULL,
	[Password] [nchar](50) NULL,
	[CreditCardID] [nchar](50) NULL,
	[TypeID] [int] NULL,
	[UserID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserTypes]    Script Date: 25-May-18 7:59:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTypes](
	[Type] [nchar](50) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_UserTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Deals]  WITH CHECK ADD  CONSTRAINT [FK_Deals_Products] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ID])
GO
ALTER TABLE [dbo].[Deals] CHECK CONSTRAINT [FK_Deals_Products]
GO
ALTER TABLE [dbo].[Deals]  WITH CHECK ADD  CONSTRAINT [FK_Deals_Users] FOREIGN KEY([SellerID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Deals] CHECK CONSTRAINT [FK_Deals_Users]
GO
ALTER TABLE [dbo].[Deals]  WITH CHECK ADD  CONSTRAINT [FK_Deals_Users1] FOREIGN KEY([Buyer])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Deals] CHECK CONSTRAINT [FK_Deals_Users1]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([Category])
REFERENCES [dbo].[Categories] ([ID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Companies] FOREIGN KEY([Brand])
REFERENCES [dbo].[Companies] ([ID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Companies]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Users] FOREIGN KEY([Seller])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UserTypes] FOREIGN KEY([TypeID])
REFERENCES [dbo].[UserTypes] ([ID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_UserTypes]
GO
USE [master]
GO
ALTER DATABASE [AutomationEngineeringDevicesStorage] SET  READ_WRITE 
GO
